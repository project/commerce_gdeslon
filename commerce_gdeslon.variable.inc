<?php

function commerce_gdeslon_variable_group_info() {
  $groups = [];

  $groups['commerce_gdeslon_settings'] = array(
    'title' => t('Gdeslon settings'),
    'access' => 'gdeslon settings',
  );

  return $groups;
}

function commerce_gdeslon_variable_info() {
  $variables = array();

  $variables['commerce_gdeslon_merchant_id'] = array(
    'title' => t('Merchant ID'),
    'group' => 'commerce_gdeslon_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 40,
    ),
    'token' => TRUE,
    'access' => 'gdeslon settings',
  );
  $variables['commerce_gdeslon_product_category_vid'] = array(
    'title' => t('Product category taxonomy vocabulary'),
    'group' => 'commerce_gdeslon_settings',
    'element' => array(
      '#type' => 'select',
    ),
    'options callback' => 'commerce_gdeslon_get_taxonomy_vocabularies_options',
    'token' => TRUE,
    'access' => 'gdeslon settings',
  );
  $variables['commerce_gdeslon_product_page_token_id'] = array(
    'title' => t('Product ID'),
    'group' => 'commerce_gdeslon_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:product-id]',
    'access' => 'gdeslon settings',
  );
  $variables['commerce_gdeslon_product_page_token_price'] = array(
    'title' => t('Product price'),
    'group' => 'commerce_gdeslon_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:commerce-price:amount-decimal]',
    'access' => 'gdeslon settings',
  );
  return $variables;
}

function commerce_gdeslon_get_taxonomy_vocabularies_options() {
  $vocabularies_info = taxonomy_vocabulary_get_names();

  $result = array();
  foreach ($vocabularies_info as $vocabulary) {
    $result[$vocabulary->vid] = $vocabulary->name;
  }
  
  return $result;
}